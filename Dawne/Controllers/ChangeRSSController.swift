//
//  ChangeRSSController.swift
//  Dawne
//
//  Created by Chenshuo  Jin on 2016-03-30.
//  Copyright © 2016 It's Too Early. All rights reserved.
//

import UIKit

class ChangeRSSController: UIViewController {
    
    var MODEL = Model.sharedInstance

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func today(action: AnyObject) {
        MODEL.rssFeed = "Today"
        dismissViewControllerAnimated(true, completion:  nil)
    }
    @IBAction func global(action: AnyObject) {
        MODEL.rssFeed = "World"
        dismissViewControllerAnimated(true, completion:  nil)
    }
    @IBAction func canada(action: AnyObject) {
        MODEL.rssFeed = "Canada"
        dismissViewControllerAnimated(true, completion:  nil)
    }
}
